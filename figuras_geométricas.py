from math import pi

def vol_cilindro(radio, altura):
    if isinstance(radio,int) or isinstance(radio,float):
        if isinstance(altura,int) or isinstance(altura,float):
            Resultado=pi*radio**2*altura
            return Resultado
        else:
            return "Error"
    else:
        return "Error"
        
        
def vol_cono(radio,altura):
    if isinstance(radio,int) or isinstance(radio,float):
        if isinstance(altura,int) or isinstance(altura,float):
            Resultado=(1/3)*pi*radio**2*altura
            return Resultado
        else:
            return "Error"
    else:
        return "Error"

def area_circulo(radio):
    if isinstance(radio,int) or isinstance(radio,float):
        area = pi*radio**2
        return area

def triangulo(base,altura):
    if isinstance(base,int) or isinstance(base,float):
        if isinstance(altura,int) or isinstance(altura,float):
            Resultado=(base*altura)/2
            return Resultado
        else:
            return "Error"
    else:
        return "Errror"

